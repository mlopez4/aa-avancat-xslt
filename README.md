# AA.2.2.3 Avancat XSLT
### Marcos Lopez Capitan

Activitat avaluable sobre tractament de fitxers xms mitjanant plantilles xsl


Contigut
======

* Exercici 1
	* Per aquest exercici hem fet servir un xml sobre [_"Actualitat de llengua catalana"_](https://analisi.transparenciacatalunya.cat/Cultura-oci/Actualitat-de-llengua-catalana/u6jd-qqzi) proporcionat per l'apartat de dades obertes de GenCat. Hem simplificat una mica la font per tal de poder ferla servir com si fos un [blog](https://gitlab.com/mlopez4/aa-avancat-xslt/blob/master/src/noticies_conversio.xsl) i reduint l'ambit geogràfic només a Catalunya.
	    * [Captura](https://gitlab.com/mlopez4/aa-avancat-xslt/blob/master/res/captures/ex_1_captura.PNG)
* Exercici 2
	* [Versio 1](https://gitlab.com/mlopez4/aa-avancat-xslt/blob/master/src/bike_rental_V1.xsl): Etiquetes traduides a l'anglés.
	    * [Captura](https://gitlab.com/mlopez4/aa-avancat-xslt/blob/master/res/captures/Ex_2_1_captura.PNG)  
	* [Versio 2](https://gitlab.com/mlopez4/aa-avancat-xslt/blob/master/src/bike_rental_V2.xsl): Llistat de revisions ordenades cronolgicament de més recent a més vella.
	    * [Captura](https://gitlab.com/mlopez4/aa-avancat-xslt/blob/master/res/captures/Ex_2_2_captura.PNG)
* Exercici 3
	* Aquest document i els [commits](https://gitlab.com/mlopez4/aa-avancat-xslt/commits/master)
* Extra
    * Directori de la [web](https://gitlab.com/mlopez4/aa-avancat-xslt/tree/master/web)
        * [Visitar Web](https://mlopez4.gitlab.io/aa-avancat-xslt/web/)
    * original [bike_rental.xml](https://gitlab.com/mlopez4/aa-avancat-xslt/blob/master/web/res/bike_rental.xml)
    * plantilla [bike\_rental_web.xsl](https://gitlab.com/mlopez4/aa-avancat-xslt/blob/master/web/bike_rental_web.xsl)
    * [index.html](https://gitlab.com/mlopez4/aa-avancat-xslt/blob/master/web/index.html)