<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <!--Llistat de revisions ordenades cronològicament de més recent a més vella-->

    <xsl:template match="/">
        <revisions>
            <xsl:for-each select="botiga/bicicletes/bicicleta/revisions/revisio">
                <xsl:sort select="data_revisio/any" order="descending"/>
                <xsl:sort select="data_revisio/mes" order="descending"/>
                <xsl:sort select="data_revisio/dia" order="descending"/>
                <xsl:apply-templates select="."/>
            </xsl:for-each>
        </revisions>

    </xsl:template>

    <xsl:template match="revisio">
        <revisio>
            <xsl:attribute name="bicicleta_id">
                <xsl:value-of select="../../@id"/>
            </xsl:attribute>
            <data>
                <xsl:value-of select="data_revisio/any"/>/<xsl:value-of select="data_revisio/mes"/>/<xsl:value-of
                    select="data_revisio/dia"/>
            </data>
            <mecanic>
                <xsl:value-of select="mecanic"/>
            </mecanic>
            <observacions>
                <xsl:value-of select="observacions"/>
            </observacions>
        </revisio>
    </xsl:template>

</xsl:stylesheet>