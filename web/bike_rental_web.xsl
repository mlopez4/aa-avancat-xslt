<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html lang="es">
            <head>
                <title>Bike Rental Web</title>
                <link rel="stylesheet" href="css/style.css"/>
                <link href="https://fonts.googleapis.com/css?family=Karla" rel="stylesheet"/>
            </head>
            <body>


                <header>
                    <h1>BIKE RENTAL WEB</h1>
                </header>

                <nav>
                    <ul>
                        <li><a href="#clients">CLIENTS</a></li>
                        <li><a href="#bicis">LES NOSTRES BICIS</a></li>
                        <li><a href="#rutes">CATALEG DE RUTES</a></li>
                    </ul>
                </nav>


                <article id="clients" class="clients-contenidor">
                    <h3>CLIENTS</h3>
                    <table>
                        <tr>
                            <th>Nom</th>
                            <th>email</th>
                        </tr>
                        <xsl:for-each select="botiga/clients">
                            <xsl:apply-templates select="."/>
                        </xsl:for-each>
                    </table>
                </article>


                <article id="bicis" class="bicis-contenidor">
                    <h3>LES NOSTRES BICIS</h3>
                    <table>
                        <tr>
                            <th>
                                Ref:
                            </th>
                            <th>
                                Model
                            </th>
                            <th>
                                Marca
                            </th>
                            <th>
                                Dara Compra:
                            </th>
                        </tr>
                        <xsl:for-each select="botiga/bicicletes">
                            <xsl:apply-templates select="."/>
                        </xsl:for-each>
                    </table>
                </article>


                <article id="rutes" class="rutes-contenidor">
                    <h3>CATALEG DE RUTES</h3>
                    <section class="rutes-subcontenidor">
                        <xsl:for-each select="botiga/rutes">
                            <xsl:apply-templates select="."/>
                        </xsl:for-each>
                    </section>
                </article>
                <footer></footer>

            </body>
        </html>
    </xsl:template>


    <xsl:template match="client">
            <tr class="client-contenidor-indv">

                <td>
                    <xsl:value-of select="nom_complet/nom"></xsl:value-of>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="nom_complet/cognom1"></xsl:value-of>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="nom_complet/cognom2"></xsl:value-of>
                </td>

                <td>
                    <xsl:value-of select="contactes/email"></xsl:value-of>
                </td>
            </tr>
    </xsl:template>


    <xsl:template match="bicicleta">
        <div class="bici-contenidor-indv">
            <tr>
                <td>
                    <xsl:value-of select="@id"></xsl:value-of>
                </td>
                <td>
                    <xsl:value-of select="model"/>
                </td>
                <td>
                    <xsl:value-of select="marca"/>
                </td>
                <td>
                    <xsl:value-of select="data_compra/any"/><xsl:text> / </xsl:text>
                    <xsl:value-of select="data_compra/mes"/><xsl:text> / </xsl:text>
                    <xsl:value-of select="data_compra/dia"/>
                </td>
            </tr>
        </div>
    </xsl:template>


    <xsl:template match="ruta">
        <div class="ruta-contenidor-indv">
            <h4>
                <xsl:value-of select="nom_ruta"/>
            </h4>
            <p>
                <xsl:value-of select="distancia"/><xsl:text> </xsl:text><xsl:value-of select="distancia/@unitat"/>
            </p>
<div class="ruta-icon-container">
            <div class="ruta-icon">
            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQgn73tnJ0zgpXTxcdhknxMu4wL0hfM9jivScYlfr6sST3JU-ws"
                 alt="Sortida" height="42" width="42"/>
            <p>
                    <xsl:value-of
                            select="sortida"/>
            </p>
        </div>

        <div class="ruta-icon">
            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcToIgblDsL4w0Ny0-kJZ38bK0iC07waadQnzNSHSOnRPzLnWmExAA"
                 alt="Sortida" height="42" width="42"/>
            <p>
                    <xsl:value-of
                            select="arribada"/>
            </p>
        </div>
</div>
            <div>
                <a>
                    <xsl:attribute name="href">
                        <xsl:value-of select="url"/>
                    </xsl:attribute>
                    Més info a Wikiloc!
                </a>
            </div>


        </div>
    </xsl:template>


</xsl:stylesheet>