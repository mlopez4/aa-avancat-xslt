<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <shop>
            <clients>
                <xsl:apply-templates select="botiga/clients"/>
            </clients>
            <bikes>
                <xsl:apply-templates select="botiga/bicicletes"/>
            </bikes>
            <rutes>
                <xsl:apply-templates select="botiga/rutes"/>
            </rutes>
            <rents>
                <xsl:apply-templates select="botiga/lloguers"/>
            </rents>
        </shop>
    </xsl:template>

    <xsl:template match="clients">
        <xsl:for-each select="client">
            <client>
                <xsl:attribute name="id">
                    <xsl:value-of select="@id"/>
                </xsl:attribute>
                <full_name>
                    <first_name>
                        <xsl:value-of select="nom_complet/nom"/>
                    </first_name>
                    <last_name>
                        <xsl:value-of select="nom_complet/cognom1"/>
                    </last_name>
                    <surname>
                        <xsl:value-of select="nom_complet/cognom2"/>
                    </surname>
                </full_name>
                <address>
                    <street>
                        <xsl:value-of select="adreca/carrer"/>
                    </street>
                    <number>
                        <xsl:value-of select="adreca/num"/>
                    </number>
                    <floor>
                        <xsl:value-of select="adreca/pis"/>
                    </floor>
                    <door>
                        <xsl:value-of select="adreca/porta"/>
                    </door>
                    <postcode>
                        <xsl:value-of select="adreca/cp"/>
                    </postcode>
                    <city>
                        <xsl:value-of select="adreca/poblacio"/>
                    </city>
                    <province>
                        <xsl:value-of select="adreca/provincia"/>
                    </province>
                </address>
                <contact>
                    <telephones>
                        <telephone>
                            <xsl:value-of select="contactes/telefons/telefon"/>
                        </telephone>
                        <telephone1>
                            <xsl:value-of select="contactes/telefons/telefon1"/>
                        </telephone1>
                        <telephone2>
                            <xsl:value-of select="contactes/telefons/telefon2"/>
                        </telephone2>
                    </telephones>
                    <email>
                        <xsl:value-of select="contactes/email"/>
                    </email>
                    <networks>
                        <xsl:copy-of select="contactes/xarxes_socials"/>
                    </networks>
                </contact>
            </client>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="bicicletes">
        <xsl:for-each select="bicicleta">
            <bike>
                <xsl:attribute name="id">
                    <xsl:value-of select="@id"/>
                </xsl:attribute>
                <brand>
                    <xsl:value-of select="marca"/>
                </brand>
                <model>
                    <xsl:value-of select="model"/>
                </model>
                <buy_date>
                    <day>
                        <xsl:value-of select="data_compra/any"/>
                    </day>
                    <month>
                        <xsl:value-of select="data_compra/mes"/>
                    </month>
                    <year>
                        <xsl:value-of select="data_compra/dia"/>
                    </year>
                </buy_date>
                <revisions>
                    <xsl:for-each select="revisions/revisio">
                        <revision>
                            <date_revision>
                                <day>
                                    <xsl:value-of select="data_revisio/dia"/>
                                </day>
                                <month>
                                    <xsl:value-of select="data_revisio/mes"/>
                                </month>
                                <year>
                                    <xsl:value-of select="data_revisio/any"/>
                                </year>
                            </date_revision>
                            <mecanic>
                                <xsl:value-of select="mecanic"/>
                            </mecanic>
                            <observations>
                                <xsl:value-of select="observacions"/>
                            </observations>
                        </revision>
                    </xsl:for-each>

                </revisions>
            </bike>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="rutes">
        <xsl:for-each select="ruta">
            <rute>
                <xsl:attribute name="id">
                    <xsl:value-of select="@id"/>
                </xsl:attribute>
                <rute_name>
                    <xsl:value-of select="nom_ruta"/>
                </rute_name>
                <start>
                    <xsl:value-of select="sortida"/>
                </start>
                <end>
                    <xsl:value-of select="arribada"/>
                </end>
                <distance>
                    <xsl:attribute name="unit">
                        <xsl:value-of select="distancia/@unitat"/>
                    </xsl:attribute>
                    <xsl:value-of select="distancia"/>
                </distance>
                <url>
                    <xsl:value-of select="url"/>
                </url>
            </rute>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="lloguers">
        <xsl:for-each select="lloguer">
            <rental>
                <xsl:attribute name="id">
                    <xsl:value-of select="@id"/>
                </xsl:attribute>
                <date>
                    <day>
                        <xsl:value-of select="data/dia"/>
                    </day>
                    <month>
                        <xsl:value-of select="data/mes"/>
                    </month>
                    <year>
                        <xsl:value-of select="data/any"/>
                    </year>
                </date>
                <id_client>
                    <xsl:value-of select="id_client"/>
                </id_client>
                <id_bike>
                    <xsl:value-of select="id_bici"/>
                </id_bike>
                <id_rute>
                    <xsl:value-of select="id_ruta"/>
                </id_rute>
            </rental>
        </xsl:for-each>
    </xsl:template>

</xsl:stylesheet>