<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <Noticies>

            <xsl:for-each select="dataroot/Noticies[Ambitgeografic='Gironès']">
                <xsl:sort order="descending" select="Data"/>
                <Noticia>
                    <Titol><xsl:value-of select="Titol"/></Titol>
                    <Data><xsl:value-of select="Data"/></Data>
                    <Text><xsl:value-of select="Text"/></Text>
                </Noticia>
            </xsl:for-each>

        </Noticies>
    </xsl:template>
</xsl:stylesheet>